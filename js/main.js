document.getElementsByClassName('burger__button')[0].onclick = function(){
    this.classList.toggle('burger__button_opened');
    document.querySelector(".dropdown-menu").classList.toggle("dropdown-menu__item_opened");
};

if (window.innerWidth < 640) {
    const navBar = document.querySelector(".header__nav-bar");
    const navBarItem = document.querySelectorAll(".nav-bar__item");

    navBar.classList.add("dropdown-menu");
    navBar.classList.remove("header__nav-bar");

    for (let i = 0; i < navBarItem.length; i++) {
        navBarItem[i].classList.add("dropdown-menu__item");
        navBarItem[i].classList.remove("nav-bar__item");
    }
}

window.addEventListener("resize", function (e) {
    const navBar = document.querySelector(".header__nav-bar");
    const dropdownBar = document.querySelector(".dropdown-menu");
    const navBarItem = document.querySelectorAll(".nav-bar__item");
    const dropdownItem = document.querySelectorAll(".dropdown-menu__item");

    if (window.innerWidth < 640) {
        if (navBar) {
            navBar.classList.add("dropdown-menu");
            navBar.classList.remove("header__nav-bar");
        }

        for (let i = 0; i < navBarItem.length; i++) {
            navBarItem[i].classList.add("dropdown-menu__item");
            navBarItem[i].classList.remove("nav-bar__item");
        }
    } else {
        if (dropdownBar) {
            dropdownBar.classList.add("header__nav-bar");
            dropdownBar.classList.remove("dropdown-menu");
        }

        for (let i = 0; i < dropdownItem.length; i++) {
            dropdownItem[i].classList.add("nav-bar__item");
            dropdownItem[i].classList.remove("dropdown-menu__item");
        }
    }
});